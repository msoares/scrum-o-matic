Scrum-O-Matic
=============

This template helps generating scrum review documents and burndown graphics
directly from plain text files. It makes it easier to maintain and keep this
kind of documentation into any version control system. Specially recommended for
those who can not bear using clumsy spreadsheets or bloated project management
tools.

Take a look at the generated output at the [link][1] below.

[1]: https://bitbucket.org/msoares/scrum-o-matic/downloads

Usage
-----

Create two files with the following names at the working directory.

    ${project name}-sprint-${sprint number}-burndown.R
    ${project name}-sprint-${sprint number}.txt

The former will be a R script file. To create the burndown data one must declare
a string called `burndown` and fill up three columns using the format below:

    stories tasks   day
    39      82      2013-07-22
    39      75      2013-07-23

After so, it's just needed to run a custom script called burndown.R as the last
statement on the file.

    source('scripts/burndown.R', local = TRUE)

The later file will be the review report, formatted with Markdown as understood
by Pandoc's markdown syntax[2] and styled by the local CSS file. It's strongly
suggested that you copy its contents from a pre-existing one.

To generate and show the new reports, type:

    make && xdg-open output/${project name}-sprint-${sprint number}.html

Currently, `graphviz` and `mscgen` files are also supported, just set the
filename extension to `.dot` or `.msc` respectively.

[2]: http://johnmacfarlane.net/pandoc/README.html#pandocs-markdown

Dependencies
------------

The document template uses `pandoc`, a general markup converter which
transforms input markdown into fancy html reports. The burndown graphics are
created by `R` + `ggplot`, advanced and open-source'd statistical and
plotting tools. Thus, the following tools are required before trying to use
the template generator.


* [pandoc](http://johnmacfarlane.net/pandoc)
* R, from the [r-base-core](http://www.r-project.org) package
* ggplot, from the [r-cran-ggplot2](http://had.co.nz/ggplot2) package


TODO
----

* implement pandoc's sliddy class
* fix up graphic burndown padding
* fixup multiple filenames mapping to the same target PNG file
* custom CSS, header and footer

<!-- vim: set filetype=markdown :-->
