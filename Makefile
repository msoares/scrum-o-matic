O ?= output
I ?= input
VPATH = $(shell pwd)

define tool-available
	$(eval $(1) := $(shell which $(2)))
	$(if $($(1)),$(info $(2) available at $($(1))),$(error error: missing tool $(2)))
endef

$(eval $(call tool-available,DOT,dot))
$(eval $(call tool-available,PANDOC,pandoc))
$(eval $(call tool-available,PYTHON,python))
$(eval $(call tool-available,RBIN,R))
$(eval $(call tool-available,MSC,mscgen))

FIGURES = $(patsubst %.R,%.png,$(wildcard $(I)/*.R))
FIGURES += $(patsubst %.dot,%.png,$(wildcard $(I)/*.dot))
FIGURES += $(patsubst %.msc,%.png,$(wildcard $(I)/*.msc))
FIGURES += $(patsubst %.py,%.png,$(wildcard $(I)/*.py))

DOCS = $(patsubst %.txt,%.html,$(wildcard $(I)/*.txt))

all: $(O) $(patsubst $(I)/%,$(O)/%,$(FIGURES) $(DOCS))

$(O):
	-mkdir -p $@

$(O)/%.png: $(I)/%.dot
	$(DOT) -Tpng -o $@ $<

$(O)/%.png: $(I)/%.msc
	$(MSC) -Tpng -o $@ $<

$(O)/%.png: $(I)/%.py
	OUTFILE=$@ $(PYTHON) $<

$(O)/%.png: $(wildcard scripts/*.R) $(I)/%.R
	OUTFILE=$@ $(RBIN) -q --no-save < $(I)/$*.R

$(O)/%-slide.html: $(I)/%-slide.txt
	$(PANDOC) -t html5 --section-divs \
		--css=scripts/reveal.css \
		--css=scripts/slide.css \
		--include-after-body=scripts/reveal.js \
		--include-after-body=scripts/reveal-footer.js \
		--self-contained -o $@ $<

$(O)/%.html: $(I)/%.txt
	$(PANDOC) --css=scripts/page.css --self-contained -o $@ $<

.PHONY: all
