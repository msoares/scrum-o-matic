Sprint 01 Review
================


Project Name   Current Sprint Number  Period
------------- ----------------------- ------------------------
My Project              01            2013-07-22 ~ 2013-08-02


Sprint Goal
------------------------------------

My fancy sprint objetive.


 #  Selected Backlog                           Planned   Done  Reason
--- ----------------------------------------- --------- ------ -----------------------
 1  The build environment workflow                5       5
 2  Build environment adaptation to platform     13      13    SDK delivered too late
 3  Core porting                                  8       0    Not tested
 4  Video output support                          5       0    Not tested
 5  Framework Support                             8       0    Not tested
                                                 39      18

Team Availability
-----------------

Role           Member Name        Planned   Actual
-------------- ----------------- --------- --------
Product Owner  Mr. Pee Oh           100      100
Scrum Team     Thy Developer        100      100
Scrum Team     Expert Intern        100      100


Sprint Result
-------------

------------------------------------------------ -------------------
Product backlog was reviewed during the sprint?  **No**
Sprint goal was achieved?                        **No**
What's next sprint goal?                         Finish this sprint
Sprint planning meeting                          2013-08-05 14h30
------------------------------------------------ -------------------

Graphical Burndown
------------------

![](output/project-sprint-01-burndown.png)
