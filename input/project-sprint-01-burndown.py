burndown = """
stories tasks   day
39      82      2013-07-22
39      75      2013-07-23
39      71      2013-07-24
39      71      2013-07-25
34      64      2013-07-26
34      64      2013-07-29
34      58      2013-07-30
21      58      2013-07-31
21      33      2013-08-01
21      29      2013-08-02
"""

from StringIO import StringIO
from matplotlib.dates import strpdate2num
from matplotlib.mlab import frange

import os
import numpy as np
import matplotlib.pyplot as plt

stories, tasks, days = np.loadtxt(StringIO(burndown),
        unpack=True,
        dtype={'names':('stories', 'tasks', 'days'), 'formats':('int', 'int', 'S10')},
        #converters={2:strpdate2num('%Y-%m-%d')},
        skiprows=2)
days = days.astype('datetime64[D]')
days = range(1, 1 + len(days))

baseline = lambda l: frange(max(l), 0, npts=len(l))

fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, sharey=False)

# Stories burndown
ax1.set_ylabel('Story Points')
#ax1.xaxis_date()
p = ax1.plot(days, stories)
ax1.fill_between(days, 0, stories, facecolor='blue', alpha=0.5)
ax1.set_xlim(days[0], days[-1])

p = ax1.plot(days, baseline(stories))
plt.setp(p, 'color', 'red')

# Activities burndown
ax2.set_ylabel('Task Points')
p = ax2.plot(days, tasks)
ax2.fill_between(days, 0, tasks, facecolor='blue', alpha=0.5)

p = ax2.plot(days, baseline(tasks))
plt.setp(p, 'color', 'red')

fig.suptitle('Sprint #1 Burndown')
#fig.autofmt_xdate()

if os.environ.has_key('OUTFILE'):
    fig.savefig(os.environ['OUTFILE'])
else:
    fig.show()
